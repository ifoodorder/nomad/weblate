WEBLATE_ENABLE_HTTPS=1
WEBLATE_REQUIRE_LOGIN=1
WEBLATE_DEBUG=1
WEBLATE_LOGLEVEL="DEBUG"
WEBLATE_SITE_TITLE="iFoodOrder"
WEBLATE_ADMIN_NAME={{ with secret "secret/weblate/admin" }}"{{ .Data.name }}"
WEBLATE_ADMIN_EMAIL="{{ .Data.email }}"
WEBLATE_ADMIN_PASSWORD="{{ .Data.password }}{{ end }}"
WEBLATE_SERVER_EMAIL="weblate@example.com"
WEBLATE_DEFAULT_FROM_EMAIL="weblate@example.com"
WEBLATE_ALLOWED_HOSTS="*"
WEBLATE_REGISTRATION_OPEN=0

POSTGRES_USER="{{ with secret "database/creds/weblate" }}{{ .Data.username }}"
POSTGRES_PASSWORD="{{ .Data.password }}{{ end }}"
POSTGRES_DATABASE="weblate"
POSTGRES_HOST=postgresql.service.consul
POSTGRES_PORT=5432

REDIS_HOST=redis.service.consul
REDIS_PORT=6379

WEBLATE_SITE_DOMAIN="weblate.ifoodorder.com"
WEBLATE_EMAIL_HOST={{ with secret "secret/weblate/email" }}{{ .Data.host }}
WEBLATE_EMAIL_HOST_USER={{ .Data.user }}
WEBLATE_EMAIL_HOST_PASSWORD={{ .Data.password }}{{ end }}
WEBLATE_EMAIL_PORT=465
WEBLATE_EMAIL_USE_TLS=0
WEBLATE_EMAIL_USE_SSL=1
WEBLATE_SOCIAL_AUTH_KEYCLOAK_KEY={{ with secret "secret/weblate/oidc" }}{{ .Data.key }}
WEBLATE_SOCIAL_AUTH_KEYCLOAK_SECRET={{ .Data.secret }}
WEBLATE_SOCIAL_AUTH_KEYCLOAK_PUBLIC_KEY={{ .Data.public_key }}
WEBLATE_SOCIAL_AUTH_KEYCLOAK_AUTHORIZATION_URL={{ .Data.auth_url }}
WEBLATE_SOCIAL_AUTH_KEYCLOAK_ACCESS_TOKEN_URL={{ .Data.token_url }}{{ end }}
WEBLATE_SOCIAL_AUTH_KEYCLOAK_ID_KEY=email

#WEBLATE_SAML_IDP_ENTITY_ID=https://weblate.ifoodorder.com/accounts/metadata/saml/
#WEBLATE_SAML_IDP_URL=https://auth.ifoodorder.com/auth/realms/master/protocol/saml/descriptor
SOCIAL_AUTH_SAML_ENABLED_IDPS=weblate

WEBLATE_NO_EMAIL_AUTH=true

BROKER_USE_SSL=true
CELERY_REDIS_BACKEND_USE_SSL=true
CELERY_REDIS_USERNAME=weblate
CELERY_REDIS_PASSWORD={{ with secret "secret/weblate/redis" }}{{ .Data.password }}{{ end }}
CELERY_REDIS_HOST=redis.service.consul
CELERY_REDIS_PORT=6379