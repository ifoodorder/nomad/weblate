job "weblate" {
    datacenters = ["dc1"]
    group "weblate" {
        network {
            port "http" {
               static = 4443
               to = 4443
             }

        }
        service {
            name = "weblate"
            port = "4443"
        }

        task "weblate" {
            vault {
                policies = ["weblate"]
            }
            resources {
                memory = 1000
                cpu = 2000
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
                ports = ["http"]
            }
            env {
                WEBLATE_SERVICE = "web"
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
        }
        task "weblate-celery-backup" {
            resources {
                cpu = 1000
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
            env {
                WEBLATE_SERVICE = "celery-backup"
            }
        }
        task "weblate-celery-beat" {
            resources {
                cpu = 500
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
            env {
                WEBLATE_SERVICE = "celery-beat"
            }
        }
        task "weblate-celery-celery" {
            resources {
                memory = 500
                cpu = 2000
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
            env {
                WEBLATE_SERVICE = "celery-celery"
            }
        }
        task "weblate-celery-memory" {
            resources {
                cpu = 1000
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
            env {
                WEBLATE_SERVICE = "celery-memory"
            }
        }
        task "weblate-celery-notify" {
            resources {
                cpu = 500
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
            }
            env {
                WEBLATE_SERVICE = "celery-notify"
            }
        }
        task "weblate-celery-translate" {
            resources {
                cpu = 500
            }
            template {
                data = file("fullchain.pem.tpl")
                destination = "local/fullchain.pem"
                perms = 666
            }
            template {
                data = file("privkey.pem.tpl")
                destination = "secrets/privkey.pem"
                perms = 666
            }
            template {
                data = file("env.tpl")
                destination = "secrets/file.env"
                env = true
            }
            driver = "docker"
            config {
                image = "weblate/weblate:4.8-1"
                volumes = [
                    "local/fullchain.pem:/app/data/ssl/fullchain.pem",
                    "secrets/privkey.pem:/app/data/ssl/privkey.pem"
                    ]
            }
            volume_mount {
                volume = "data"
                destination = "/app/data"
            }
            env {
                WEBLATE_SERVICE = "celery-translate"
            }
        }
        volume "data" {
            type = "host"
            source = "weblate"
            read_only = false
        }
    }
}
