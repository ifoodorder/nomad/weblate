{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=weblate" "common_name=weblate.service.consul" "ttl=24h" "alt_names=_weblate._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.ca_chain }}
{{ .Data.certificate }}
{{ end }}{{ end }}